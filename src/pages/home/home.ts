import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {FCM} from "@ionic-native/fcm";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {Clipboard} from "@ionic-native/clipboard";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    token: string = '';
    notifications: any[] = [];


    constructor(public navCtrl: NavController,
                public ln: LocalNotifications,
                public cb: Clipboard,
                public fcm: FCM) {

        this.fcm.getToken().then(token => {
            this.token = token;
        });
        this.fcm.onNotification().subscribe(data => {
            this.notifications.push(JSON.stringify(data));

            if (data.wasTapped) {
                alert("Received in background");
            } else {
                alert("Received in foreground");
            }
        })
    }

    clearNotification() {
        this.ln.clearAll();
    }

    giveNotification() {
        this.ln.schedule({
            id: 1,
            text: 'Single ILocalNotification',
            // sound: isAndroid ? 'file://sound.mp3' : 'file://beep.caf',
            data: {
                'yoyoyoyyo': 'yoyooyyo'
            }
        });
    }

    copyToken() {
        if (!this.token) {
            alert("there's nothing to copy.");
            return;
        }

        this.cb.copy(this.token).then(() => {
            alert('copied');
        });
    }

    giveMultipleNotification() {
        this.ln.schedule({
            id: Math.round(Math.random() * 99),
            text: 'Multiple ILocalNotification',
            // sound: isAndroid ? 'file://sound.mp3' : 'file://beep.caf',
            data: {},
            badge: Math.round(Math.random() * 99),
            title: 'hello title',
        });
    }

    giveStickyNotification() {
        this.ln.schedule({
            id: Math.round(Math.random() * 99),
            text: 'Multiple ILocalNotification',
            // sound: isAndroid ? 'file://sound.mp3' : 'file://beep.caf',
            data: {},
            badge: Math.round(Math.random() * 99),
            title: 'hello title',
            sticky: true,
        });
    }

}
